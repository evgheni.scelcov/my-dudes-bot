import axios from 'axios';
import { createTelegramEndpointURL } from 'utils';

const { CHAT_ID, TELEGRAM_TOKEN } = process.env;

export class DudesTelegramBotApi {
  private readonly chatID: number;
  private readonly token: string;
  private readonly today: string;

  constructor() {
    this.chatID = +CHAT_ID;
    this.token = TELEGRAM_TOKEN;
  }

  async sendPhoto(photo: string) {
    await axios.post(createTelegramEndpointURL(this.token, 'sendPhoto'), {
      photo,
      chat_id: this.chatID,
    });
  }

  async sendMessage(text: string) {
    await axios.post(createTelegramEndpointURL(this.token, 'sendMessage'), {
      text,
      chat_id: this.chatID,
    });
  }
}
