import DateTime from 'luxon/src/datetime.js';

export function getWeekdayNumber(): string {
  return DateTime.local().setZone('Europe/Chisinau').weekdayLong;
}
