import { AWS_CONSTANTS } from '_constants';

export function createS3ImageURL(day: string): string {
  return `${AWS_CONSTANTS.BUCKET_NAME}${day}${Math.floor(Math.random() * 2) + 1}.jpg`;
}
