import { endpointTypes } from 'types';

export function createTelegramEndpointURL(token: string, type: endpointTypes): string {
  return `https://api.telegram.org/bot${token}/${type}`;
}
