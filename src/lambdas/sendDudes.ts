import { COMMANDS, DAYS_TO_SEND, lambdaResponses } from '_constants';
import { DudesTelegramBotApi } from 'helpers';
import { createS3ImageURL, getWeekdayNumber } from 'utils';

async function sendDudes(event) {
  try {
    const {
      message: { text },
    } = JSON.parse(event.body);
    const bot = new DudesTelegramBotApi();
    const today = getWeekdayNumber();
    if (text.startsWith(COMMANDS.SENDDUDES) && DAYS_TO_SEND.includes(today)) {
      await bot.sendPhoto(createS3ImageURL(today));
    } else {
      await bot.sendMessage('Плавают 2 гея в бассейне, один видит - сперма всплыла и спрашивает у второго: "Ты кончил?", а тот ему отвечает: "Нет, пернул"');
    }
    return lambdaResponses.success;
  } catch (e) {
    // logs for CloudWatch
    console.log(e.message);
    // I always return 200 here because I don't want telegram to resend failed messages
    return lambdaResponses.success;
  }
}

export { sendDudes };
