import { lambdaResponses } from '_constants';
import { DudesTelegramBotApi } from 'helpers';
import { createS3ImageURL, getWeekdayNumber } from 'utils';

async function dude() {
  try {
    const bot = new DudesTelegramBotApi();
    await bot.sendPhoto(createS3ImageURL(getWeekdayNumber()));
    return lambdaResponses.success;
  } catch (e) {
    // logs for CloudWatch
    console.log(e.message);
    //  I always return 200 here because I don't want telegram to resend failed messages
    return lambdaResponses.success;
  }
}

export { dude };
