const slsw = require('serverless-webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'development',
    devtool: false,
    target: 'node',
    entry: slsw.lib.entries,
    stats: 'minimal',
    resolve: {
        extensions: ['.js', '.ts'],
        modules: ['node_modules', 'src']
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, '.webpack'),
        filename: '[name].js'
    },
    optimization: {
        minimize: true,
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            { test: /\.ts?$/, loader: 'ts-loader' }
        ]
    },
};